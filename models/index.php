<?php 
include 'autoload.php';

if ($_SERVER['REQUEST_METHOD'] === 'GET')
{
  	
  if (isset($_SESSION['message']))
  {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
  }
   
  $order = new Order;  
 	include 'form.html';


}

elseif ($_SERVER['REQUEST_METHOD'] === 'POST')
{

  $order = new order($_POST);
 
  if ($order->save())
  {
    $_SESSION['message'] = 'Спасибо за регистрацию, ждем Вас 14 июня на открытии школы!';
    header('Location: ' . '/');
    exit();
  }
  else
  {
    include 'form.html';
  }
}