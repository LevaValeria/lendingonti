<?php
/**
 * Created by PhpStorm.
 * User: DNS
 * Date: 06.03.2019
 * Time: 13:53
 */

//    require_once "PDO_connect.php";

//JSON EXAMPLE
//{
//      "lon":52.28,
//		"lat":124.24
//}

    function getRequestHeaders() {
        $headers = array();
        foreach($_SERVER as $key => $value) {
            if (substr($key, 0, 5) <> 'HTTP_') {
                continue;
            }
            $header = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
            $headers[$header] = $value;
        }
        return $headers;
    }

    $json_str = file_get_contents('php://input');
    $json_obj = json_decode($json_str);


    $headers = getRequestHeaders();


    $json_obj->id_user = $headers['Device-Id'];
    $json_obj->id_group = $headers['Group-Id'];



    $array_response = array();
    if(!isset($json_obj->id_user)){
        $array_response["error 1:"] = array("empty user id");
    }
    if(!isset($json_obj->id_group)){
        $array_response["error 2:"] = array("empty group id");
    }
    if(!isset($json_obj->lon)){
        $array_response["error 4:"] = array("empty lon");
    }
    if(!isset($json_obj->lat)){
        $array_response["error 5:"] = array("empty lat");
    }

    if(!empty($array_response)){
        $json_response = json_encode($array_response);
        echo $json_response;
        exit();
    }
    require_once "PDO_connect.php";
    update_coords($pdo, $json_obj);









