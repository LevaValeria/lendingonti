<?php 

define('DB_HOST', 'localhost');
define('DB_NAME', 'leto');
define('DB_USER', 'root');
define('DB_PASS', 'root');
define('DB_CHAR', 'utf8');



class Database {
  
  	
    protected static $_pdo;

    public static function get_pdo()
    {
        if (empty(static::$_pdo))
        {
           $dsn =  'mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset='.DB_CHAR;
    			 $opt = [
        				PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        				PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        				PDO::ATTR_EMULATE_PREPARES   => false,
   							 ];
  
         				static::$_pdo = new PDO($dsn, DB_USER, DB_PASS, $opt);;
        }

        return static::$_pdo;
    }

}