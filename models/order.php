<?

class Order {
	
  public $id;
  public $name;
  public $middlename;
  public $lastname;
  public $email;
  public $phone;
  public $age;
  public $course_id;
  protected $errors = [];
  
  protected static $table = 'orders';
  protected static $attributes = 
  [
    'name',
    'middlename',
    'lastname',
    'email',
    'phone',
    'age',
    'course_id', 
  	'created_at',
  ];

  public static $array_courses = 
  [
    1 => 'Студия мультипликации', 
    2 => 'Каллиграфия и леттеринг', 
    3 => 'Графика. Книжные иллюстрации',
    4 => 'Фотография', 
    5 => 'Minecraft 1: программируй свой мир', 
    6 => 'Minecraft 2: создание модов',
    7 => 'Программирование мобильных устройств для начинающих', 
    8 => 'Умные вещи: робототехника на платформе Arduino', 
    9 => 'Основы 3D моделирования. Работа с 3D принтерами',
    10 => 'Аниматроники: роботы для начинающих', 
    11 => 'Робототехника на основе конструктора HUNA-MRT',
    12 => 'Программирование для детей',
  ];


  public function __construct($data = null)
  {
    if (isset($data) && is_array($data))
    {
    	$this->name = isset($data['name']) ? $data['name'] : "" ;
      $this->lastname = isset($data['lastname']) ? $data['lastname'] : "" ;
      $this->middlename = isset($data['middlename']) ? $data['middlename'] : "" ;
      $this->email = isset($data['email']) ? $data['email'] : "" ;
      $this->phone = isset($data['phone']) ? $data['phone'] : "" ;
      $this->age = isset($data['age']) ? $data['age'] : "" ;
      $this->course_id = isset($data['course_id']) ? $data['course_id'] : "" ;
  	}

  }
    
    
  protected static function get_pdo()
   {
    	return Database::get_pdo();
   } 
  
  public function get_config ()
  {
    return $config = [
      'name' => $this->name,
      'lastname'=> $this->lastname,
      'middlename'=> $this->middlename,
      'email' => $this->email,
      'phone' => $this->phone,
      'age' => $this->age,
      'course_id' => $this->course_id,
      'created_at'=> date('d-m-Y H:i:s'),
    ];
  }
    
    
  
  public function save()
  {
    
      $sql = static::get_pdo()->prepare('INSERT INTO `' . static::$table . '` (`' . implode('`, `', static::$attributes) . '`) VALUES (:' . implode(', :', static::$attributes) . ');');

      $data = [];
      $config = $this->get_config();

      foreach (static::$attributes as $attribute)
      {
       	$data[$attribute] = $config[$attribute];
      }

      $sql->execute($data);
			return $sql->rowCount() === 1;
    
  }
    

  public function get_course()
  {
    return Order::$array_courses[$this->course_id];
  }

    
}