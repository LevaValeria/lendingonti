<?php
/**
 * Created by PhpStorm.
 * User: DNS
 * Date: 05.03.2019
 * Time: 13:17
 */


//----Данные для подключения-------------
$db_server = "127.0.0.1";
$user = "root";
$pass = "cevthrb";
$db_name = "nti";


try {
    $pdo = new PDO("mysql:host=$db_server;dbname=$db_name", $user, $pass);
} catch (PDOException $e) {
    die($e->getMessage());
}


function insert_row_groups($pdo, $name, $phone, $group_number){
    $sth = $pdo->prepare("INSERT INTO `groups` (`id`, `name`, `phone`, `group_id`, `timestamp`)
                                       VALUES (NULL, :name, :phone, :group_number, CURRENT_TIMESTAMP)");


    $sth->bindParam(':name', $name, PDO::PARAM_STR, 255);
    $sth->bindParam(':phone', $phone, PDO::PARAM_STR, 255);
    $sth->bindParam(':group_number', $group_number, PDO::PARAM_INT, 3);
    $sth->execute();
}

function get_max_group_id($pdo){
    $sth = $pdo->prepare("SELECT MAX(`group_id`) FROM `groups`");
    $sth->execute();
    $row = $sth->fetch();
    return $row[0];
}

function check_prev_rows($pdo){
    $sth = $pdo->prepare("SELECT `timestamp` FROM `groups` LIMIT 1");
    $sth->execute();
    $row = $sth->fetch()[0];
    $date = new DateTime($row);
    $cur_date = new DateTime("now");

    $dteDiff  = $date->diff($cur_date);
    if($dteDiff->days > 0){
        delete_prev_rows($pdo);
    }
}

function delete_prev_rows($pdo){
    $sth = $pdo->prepare("TRUNCATE TABLE `groups`");
    $sth->execute();
}

function update_coords($pdo, $json_object){
    $sth = $pdo->prepare("INSERT INTO `coords` (`id`, `id_user`, `id_group`, `lat`, `lon`, `created_at`)
                                       VALUES (NULL, :id_user, :id_group, :lat, :lon, CURRENT_TIMESTAMP)");
    if (is_set_user_coords($pdo, $json_object->id_user, $json_object->id_group)){
        $sth = $pdo->prepare("UPDATE `coords` SET `lat` = :lat, `lon` = :lon WHERE `id_user` = :id_user AND `id_group` = :id_group LIMIT 1");
        echo "update";
    }

    $sth->bindParam(':id_user', $json_object->id_user, PDO::PARAM_INT, 10);
    $sth->bindParam(':id_group', $json_object->id_group, PDO::PARAM_INT, 10);
    $sth->bindParam(':lat', $json_object->lat, PDO::PARAM_STR, 255);
    $sth->bindParam(':lon', $json_object->lon, PDO::PARAM_STR, 255);

    $sth->execute();
}

function is_set_user_coords($pdo, $id_user, $id_group){
    $sth = $pdo->prepare("SELECT `id` FROM `coords` WHERE `id_user` = :id_user AND `id_group` = :id_group LIMIT 1");
    $sth->bindParam(':id_user', $id_user, PDO::PARAM_INT, 10);
    $sth->bindParam(':id_group', $id_group, PDO::PARAM_INT, 10);
    $sth->execute();
    return !(empty($sth->fetchAll()));
}

function get_group_coords($pdo, $id_group){

}

function get_user_coords($pdo, $id_user){

}

function get_coords_group_by_groups($pdo){
    $groups = array();
    $sql= "SELECT `id_group`, `lat`, `lon` FROM `coords` WHERE `deleted` = false ORDER BY `id_group`";
    $last_id = 0;

    $arr = array();
    $arr['members'] = array();

    $last_id = -1;


    foreach ($pdo->query($sql) as $row) {
        $group_id = $row['id_group'];
        $member = array(
                        'lat'=>$row['lat'],
                        'lon'=>$row['lon']);


        if($last_id !== $group_id){
            array_push($groups, $arr);
            $arr = array();
            $arr['members'] = array();
        }
        $arr['groupId'] = $group_id;

        array_push($arr['members'], $member);

        $last_id = $group_id;

    }
    array_push($groups, $arr);
    array_shift($groups);
//var_dump($groups);
    return json_encode($groups);

}