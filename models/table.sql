CREATE TABLE IF NOT EXISTS `orders` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lastname` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `middlename` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `age` int(10) NOT NULL,
  `course_id` int(10) NOT NULL,
  `created_at` TIMESTAMP NOT NULL,
  PRIMARY KEY(`id`),
  INDEX `course_id` (`course_id`),
);

CREATE TABLE `courses` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY(`id`)
);

INSERT INTO `courses` (`name`) VALUES
('Студия мультипликации'), 
 ('Каллиграфия и леттеринг'), 
('Графика. Книжные иллюстрации'),
('Фотография'), 
 ('Minecraft 1: программируй свой мир'), 
 ('Minecraft 2: создание модов'),
 ('Программирование мобильных устройств для начинающих'), 
 ('Умные вещи: робототехника на платформе Arduino'), 
 ('Основы 3D моделирования. Работа с 3D принтерами'),
 ('Аниматроники: роботы для начинающих'), 
 ('Робототехника на основе конструктора HUNA-MRT'),
 ('Программирование для детей');
